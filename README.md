# MCSS YunoHost Edition
How to create a server very easy without following tutorials? This is MCSS. Works with Python on Linux (but will be expended on every platforms), create a Minecraft server for you and your friends!

**Shipped version:** 4.5

You can get further information on the following repository : https://gitea.com/chopin42/MCSS

## Install with terminal

Run the following command in the /home/ directory :

```
sudo yunohost app install https://gitea.com/chopin42/MCSS_ynh
```

*You cannot install this app through the graphic interface*

## Uninstall with terminal

*You can remove the app from the graphic interface*

```
sudo yunohost app remove mcss
```
